<?php

/**
 * @file
 * The default format for adresses.
 */

$plugin = array(
  'title' => t('Norwegian personal identification number'),
  'format callback' => 'daddress_format_norwegian_pid',
  'type' => 'address',
  'weight' => -100,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function daddress_format_norwegian_pid(&$format, $address, $context = array()) {
  // If country is Norway.
  if ($address['country'] == 'NO') {
    // Use the administrative_area for the pid.
    $format['locality_block']['administrative_area'] = array(
      '#title' => t('Personal identification number'),
      '#size' => 30,
      '#required' => TRUE,
      '#tag' => 'div',
      '#prefix' => '',
      '#attributes' => array('class' => array('norwegian-pid')),
    );
    // Sort out the display of the pid.
    if ($context['mode'] == 'render') {
      // Create an address block for the PID.
      $format['norwegian_pid_block'] = array(
        '#type' => 'addressfield_container',
        '#attributes' => array('class' => array('norwegian-pid-block')),
        '#weight' => 150,
        '#prefix' => sprintf('<div class="field-label">%s:</div>',
                t('Personal identification number')),
      );
      // Move the PID into the new address block.
      $format['norwegian_pid_block']['administrative_area']
        = $format['locality_block']['administrative_area'];
      unset($format['locality_block']['administrative_area']);
    }
  }
}
